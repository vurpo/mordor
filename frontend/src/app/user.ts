export class User {
    id: number;
    name: string;
    balance: number;
}

export class NewUser {
    name: string;
    balance: number;
}

export class Card {
    code: string;
    user_id: number;
}