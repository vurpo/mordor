import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product, NewProduct } from './product';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ProductService {
  private getProductsUrl = 'http://localhost:8000/products';
  private adminProductsUrl = 'http://localhost:8000/admin/product';

  constructor(
    private http: HttpClient) { }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.getProductsUrl)
      .pipe(
        tap(products => this.log(`fetched products`)),
        catchError(this.handleError('getProducts', []))
      );
  }

  addProduct(product: NewProduct): Observable<Product> {
    return this.http.post<Product>(this.adminProductsUrl, product, httpOptions)
      .pipe(
        tap((product: Product) => this.log(`added product w/ id=${product.id}`)),
        catchError(this.handleError<Product>('addProduct'))
      );
  }

  updateProduct(product: Product): Observable<any> {
    const url = `${this.adminProductsUrl}/${product.id}`

    return this.http.put(url, product, httpOptions).pipe(
      tap(_ => this.log(`updated product id=${product.id}`)),
      catchError(this.handleError<any>('updateProduct'))
    );
  }

  deleteProduct (product: Product | number): Observable<Product> {
    const id = typeof product === 'number' ? product : product.id;
    const url = `${this.adminProductsUrl}/${id}`;
 
    return this.http.delete<Product>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted product id=${id}`)),
      catchError(this.handleError<Product>('deleteHero'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.log(error);
 
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log("ProductService:"+  message);
  }
}
