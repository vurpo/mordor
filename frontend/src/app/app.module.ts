import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http'; 
import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { AppRoutingModule } from './/app-routing.module';
import { ProductService } from './product.service';
import { UserService } from './user.service';
import { FormsModule } from '@angular/forms';
import { UserAdminComponent } from './user-admin/user-admin.component';
import { StorefrontComponent } from './storefront/storefront.component';
import { ShoppingcartComponent } from './shoppingcart/shoppingcart.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    UserAdminComponent,
    StorefrontComponent,
    ShoppingcartComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [
    ProductService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
