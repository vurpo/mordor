import { Component, OnInit } from '@angular/core';

import { Product, NewProduct } from '../product';
import { ProductService } from '../product.service';
import { User, NewUser } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-storefront',
  templateUrl: './storefront.component.html',
  styleUrls: ['./storefront.component.css']
})
export class StorefrontComponent implements OnInit {
  
  shoppingCartOpened: boolean = false;
  shoppingCart: Product[];
  currentUser: User;

  products: Product[];

  getProducts(): void {
    this.productService.getProducts()
      .subscribe(products => this.products = products);
  }

  constructor(
    private productService: ProductService,
    private userService: UserService) { }

  ngOnInit() {
    this.getProducts();
  }

}
