import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User, NewUser, Card } from './user';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UserService {
  private baseUrl = 'http://localhost:8000'
  private getUsersUrl = 'http://localhost:8000/admin/users';
  private adminUsersUrl = 'http://localhost:8000/admin/users';

  constructor(
    private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.getUsersUrl)
      .pipe(
        tap(users => this.log(`fetched Users`)),
        catchError(this.handleError('getUsers', []))
      );
  }

  addUser(user: NewUser): Observable<User> {
    return this.http.post<User>(this.adminUsersUrl, user, httpOptions)
      .pipe(
        tap((user: User) => this.log(`added User w/ id=${user.id}`)),
        catchError(this.handleError<User>('addUser'))
      );
  }

  updateUser(user: User): Observable<any> {
    const url = `${this.adminUsersUrl}/${user.id}`

    return this.http.put(url, user, httpOptions).pipe(
      tap(_ => this.log(`updated User id=${user.id}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

  deleteUser (user: User | number): Observable<User> {
    const id = typeof user === 'number' ? user : user.id;
    const url = `${this.adminUsersUrl}/${id}`;
 
    return this.http.delete<User>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted User id=${id}`)),
      catchError(this.handleError<User>('deleteHero'))
    );
  }

  addCard(code: string, user: User | number): Observable<any> {
    const id = typeof user === 'number' ? user : user.id;
    const url = `${this.baseUrl}/admin/cards`

    return this.http.post(url, {code: code, user_id: id}, httpOptions).pipe(
      tap(_ => this.log(`added new Card`)),
      catchError(this.handleError('addCard'))
    );
  }

  deleteCard(code: string): Observable<any> {
    const url = `${this.baseUrl}/admin/cards/${code}`

    return this.http.delete(url, httpOptions).pipe(
      tap(_ => this.log('deleted Card')),
      catchError(this.handleError('deleteCard'))
    );
  }

  getCards(user: User | number): Observable<Card[]> {
    const id = typeof user === 'number' ? user : user.id;
    const url = `${this.baseUrl}/admin/users/${id}/cards`

    return this.http.get<Card[]>(url, httpOptions)
      .pipe(
        tap(_ => this.log(`fetched cards`)),
        catchError(this.handleError('getCards', []))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.log(error);
 
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log("UserService:"+  message);
  }
}
