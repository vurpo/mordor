import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User, Card } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: ['./user-admin.component.css']
})
export class UserAdminComponent implements OnInit {
  Math = Math;

  @Input() user: User;
  @Input() userService: UserService;

  @Output() done: EventEmitter<User> = new EventEmitter();
  @Output() cancel: EventEmitter<any> = new EventEmitter();
  @Output() deleteUser: EventEmitter<any> = new EventEmitter();

  cards: Card[];
  newCard: string = "";

  constructor() { }

  ngOnInit() {
    this.getCards();
  }

  getCards() {
    this.userService.getCards(this.user)
      .subscribe(cards => this.cards = cards);
  }

  deleteCard(card: Card) {
    this.userService.deleteCard(card.code)
      .subscribe(_ => this.getCards());
  }

  addCard() {
    this.userService.addCard(this.newCard, this.user)
      .subscribe(_ => this.getCards());
  }

  okEditor() {
    this.done.emit(this.user);
  }

  cancelEditor() {
    this.cancel.emit(null);
  }

  deleteEditor() {
    this.deleteUser.emit(null);
  }

}
