import { Component, OnInit, Input, Output } from '@angular/core';
import { ProductService } from '../product.service';
import { Product, NewProduct } from '../product';
import { UserService } from '../user.service';
import { User, NewUser } from '../user';

@Component({
  selector: 'app-shoppingcart',
  templateUrl: './shoppingcart.component.html',
  styleUrls: ['./shoppingcart.component.css']
})
export class ShoppingcartComponent implements OnInit {

  @Input() user: User;

  constructor(
    private productService: ProductService,
    private userService: UserService) { }

  ngOnInit() {
  }

}
