import { Component, OnInit } from '@angular/core';

import { Product, NewProduct } from '../product';
import { ProductService } from '../product.service';
import { User, NewUser } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  Math = Math;

  products: Product[];
  selectedProduct: Product;
  newProduct: NewProduct = {
    name: "",
    code: "",
    price: 0
  };

  users: User[];
  selectedUser: User;
  newUser: NewUser = {
    name: "",
    balance: 0
  };

  constructor(
    private productService: ProductService,
    private userService: UserService) { }
  
  ngOnInit() {
    this.getProducts();
    this.getUsers();
  }

  getProducts(): void {
    this.productService.getProducts()
      .subscribe(products => this.products = products);
  }

  addProduct() {
    this.productService.addProduct(this.newProduct)
      .subscribe(r=>this.getProducts());
  }

  deleteProduct(product: Product) {
    this.productService.deleteProduct(product)
      .subscribe(r=>this.getProducts());
  }

  getUsers() {
    this.userService.getUsers()
      .subscribe(users => this.users = users);
  }

  addUser() {
    this.userService.addUser(this.newUser)
      .subscribe(r=>this.getUsers());
  }

  selectUser(user: User) {
    this.selectedUser = user;
  }

  updateUser(user: User) {
    this.userService.updateUser(user)
      .subscribe(r=>{
        this.getUsers();
        this.unselectUser();
      });
  }

  unselectUser() {
    this.selectedUser = null;
    this.getUsers();
  }

  deleteUser(user: User) {
    this.userService.deleteUser(user)
      .subscribe(r=>this.getUsers());
  }
}
