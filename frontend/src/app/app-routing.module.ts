import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminComponent } from './admin/admin.component';
import { StorefrontComponent } from './storefront/storefront.component';

const routes: Routes = [
    { path: 'admin', component: AdminComponent },
    { path: '', component: StorefrontComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ],
})
export class AppRoutingModule { }
