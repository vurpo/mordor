export class Product {
    id: number;
    name: string;
    code: string;
    price: number;
}

export class NewProduct {
    name: string;
    code: string;
    price: number;
}