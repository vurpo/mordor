use diesel;
use diesel::prelude::*;
use dotenv::dotenv;
use r2d2;
use r2d2_diesel::ConnectionManager;
use std::env;

use models;
use errors::*;

// Based on Rocket's r2d2+diesel example code

use std::ops::Deref;
use rocket::http::Status;
use rocket::request::{self, FromRequest};
use rocket::{Request, State, Outcome};

type Pool = r2d2::Pool<ConnectionManager<SqliteConnection>>;

pub struct DbConn(pub r2d2::PooledConnection<ConnectionManager<SqliteConnection>>);

/// Attempts to retrieve a single connection from the managed database pool. If
/// no pool is currently managed, fails with an `InternalServerError` status. If
/// no connections are available, fails with a `ServiceUnavailable` status.
impl<'a, 'r> FromRequest<'a, 'r> for DbConn {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<DbConn, ()> {
        let pool = request.guard::<State<Pool>>()?;
        match pool.get() {
            Ok(conn) => Outcome::Success(DbConn(conn)),
            Err(_) => Outcome::Failure((Status::ServiceUnavailable, ()))
        }
    }
}

// For the convenience of using an &DbConn as an &SqliteConnection.
impl Deref for DbConn {
    type Target = diesel::sqlite::SqliteConnection;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// Initializes a database pool.
pub fn init_pool() -> Result<Pool> {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL")
        .chain_err(|| "DATABASE_URL is not set")?;
    let manager = ConnectionManager::<SqliteConnection>::new(database_url);
    r2d2::Pool::builder()
        .max_size(1) // pool size is just 1, because an SQLite database can not be accessed concurrently
        .build(manager)
        .chain_err(|| "could not create database connection pool")
}

/// Returns all products stored in the database.
pub fn get_all_products(conn: &SqliteConnection) -> Result<Vec<models::Product>> {
    use schema::products::dsl::*;

    products.load::<models::Product>(conn).map(|s| {
        s.into_iter().map(|i| i.into()).collect()
    })
        .chain_err(|| "failed to get products from database")
}

pub fn get_all_users(conn: &SqliteConnection) -> Result<Vec<models::User>> {
    use schema::users::dsl::*;

    users.load::<models::User>(conn).map(|s| {
        s.into_iter().map(|i| i.into()).collect()
    })
        .chain_err(|| "failed to get users from database")
}

pub fn get_user_by_card(conn: &SqliteConnection, card_code: String) -> Result<models::User> {
    use schema::users::dsl::*;
    use schema::user_cards::dsl::*;

    let card = user_cards
        .filter(code.eq(card_code))
        .first::<models::Card>(conn)
        .chain_err(|| "failed to get card from database")?;

    users
        .filter(id.eq(card.user_id))
        .first::<models::User>(conn)
        .chain_err(|| "failed to get user by user id")
}

pub fn create_product(conn: &SqliteConnection, product: models::NewProduct) -> Result<models::Product> {
    use schema::products::dsl::*;

    diesel::insert_into(products)
        .values(&product)
        .execute(conn)
        .chain_err(|| "failed to insert product into database")?;

    products
        .order(id.desc())
        .first::<models::Product>(conn)
        .chain_err(|| "failed to get the inserted row from database")
}

pub fn get_product_info_by_code(conn: &SqliteConnection, product_code: String) -> Result<models::Product> {
    use schema::products::dsl::*;

    products
        .filter(code.eq(product_code))
        .first::<models::Product>(conn)
        .chain_err(|| "failed to get product by product code")
}

pub fn get_product_info(conn: &SqliteConnection, product_id: i32) -> Result<models::Product> {
    use schema::products::dsl::*;

    products
        .filter(id.eq(product_id))
        .first::<models::Product>(conn)
        .chain_err(|| "failed to get product by id")
}

pub fn update_product(conn: &SqliteConnection, product_id: i32, product: models::NewProduct) -> Result<models::Product> {
    use schema::products::dsl::*;

    diesel::update(products.find(product_id))
        .set(&product)
        .execute(conn)
        .chain_err(|| "failed to update the row in the database")?;

    products
        .filter(id.eq(product_id))
        .first::<models::Product>(conn)
        .chain_err(|| "failed to get updated row from database")
}

pub fn delete_product(conn: &SqliteConnection, product_id: i32) -> Result<()> {
    use schema::products::dsl::*;

    diesel::delete(
        products.filter(
            id.eq(product_id)))
        .execute(conn)
        .chain_err(|| "failed to delete product from database")?;

    Ok(())
}

pub fn deduct_money_amount(conn: &SqliteConnection, user_id: i32, amount: i32) -> Result<()> {
    use schema::users::dsl::*;

    conn.transaction::<_,_,_>(|| {
        let current_balance = users
            .filter(id.eq(user_id))
            .first::<models::User>(conn)
            .chain_err(|| "failed to get user from database")?
            .balance;

        let new_balance = current_balance.checked_sub(amount)
            .chain_err(|| "integer underflow when deducting money")?;

        diesel::update(users.find(user_id))
            .set(balance.eq(new_balance))
            .execute(conn)
            .chain_err(|| "failed to update the balance in the database")
    });

    Ok(())
}

pub fn create_user(conn: &SqliteConnection, user: models::NewUser) -> Result<models::User> {
    use schema::users::dsl::*;

    diesel::insert_into(users)
        .values(&user)
        .execute(conn)
        .chain_err(|| "failed to insert user into database")?;

    users
        .order(id.desc())
        .first::<models::User>(conn)
        .chain_err(|| "failed to get the inserted row from database")
}

pub fn update_user(conn: &SqliteConnection, user_id: i32, user: models::NewUser) -> Result<models::User> {
    use schema::users::dsl::*;

    diesel::update(users.find(user_id))
        .set(&user)
        .execute(conn)
        .chain_err(|| "failed to update the row in the database")?;

    users
        .filter(id.eq(user_id))
        .first::<models::User>(conn)
        .chain_err(|| "failed to get updated row from database")
}

pub fn delete_user(conn: &SqliteConnection, user_id: i32) -> Result<()> {
    use schema::users::dsl::*;

    diesel::delete(
        users.filter(
            id.eq(user_id)))
        .execute(conn)
        .chain_err(|| "failed to delete user from database")?;

    Ok(())
}

pub fn create_card(conn: &SqliteConnection, card: models::Card) -> Result<()> {
    use schema::user_cards::dsl::*;

    diesel::insert_into(user_cards)
        .values(&card)
        .execute(conn)
        .chain_err(|| "failed to insert card into database")?;

    Ok(())
}

pub fn delete_card(conn: &SqliteConnection, card_code: String) -> Result<()> {
    use schema::user_cards::dsl::*;

    diesel::delete(
        user_cards.filter(
            code.eq(card_code)))
        .execute(conn)
        .chain_err(|| "failed to delete card from database")?;

    Ok(())
}

pub fn get_cards_by_user(conn: &SqliteConnection, id: i32) -> Result<Vec<models::Card>> {
    use schema::user_cards::dsl::*;

    user_cards.filter(user_id.eq(id))
        .load::<models::Card>(conn)
        .chain_err(|| "failed to get cards from database")
}
