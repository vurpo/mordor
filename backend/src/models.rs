use super::schema::*;

/// Product as retrieved from the database
#[derive(Queryable, Serialize)]
pub struct Product {
    pub id: i32,
    pub name: String,
    pub code: String,
    pub price: i32,
}

/// New product to be inserted into the database
#[derive(Insertable, Deserialize, AsChangeset)]
#[table_name="products"]
pub struct NewProduct {
    pub name: String,
    pub code: String,
    pub price: i32,
}

/// User as retrieved from the database
#[derive(Identifiable, Queryable, Serialize)]
pub struct User {
    pub id: i32,
    pub name: String,
    pub balance: i32,
}

/// New user to be inserted into the database
#[derive(Insertable, Deserialize, AsChangeset)]
#[table_name="users"]
pub struct NewUser {
    pub name: String,
    pub balance: i32,
}

/// Card as retrieved from the database
#[derive(Identifiable, Queryable, Insertable, AsChangeset, Associations, Serialize, Deserialize)]
#[primary_key(code)]
#[belongs_to(User)]
#[table_name="user_cards"]
pub struct Card {
    pub code: String,
    pub user_id: i32,
}
