    #![feature(plugin)]
#![plugin(rocket_codegen)]

// `error_chain!` can recurse deeply
#![recursion_limit = "1024"]

// error handling library
#[macro_use] extern crate error_chain;

// database library
#[macro_use] extern crate diesel;
#[macro_use] extern crate diesel_infer_schema;
extern crate dotenv;
extern crate r2d2_diesel;
extern crate r2d2;

// JSON serialiser/deserialiser
#[macro_use] extern crate serde_derive;
extern crate serde_json;

// HTTP server framework
extern crate rocket;
extern crate rocket_contrib;
extern crate rocket_cors;

// contains the database schemas
pub mod schema;
// data models (for database and REST API)
pub mod models;
// functions for interacting with database
pub mod db;
// tests
#[cfg(test)] mod tests;

use rocket::fairing::{
    Fairing,
    Info,
    Kind
};
use rocket_cors::{AllowedOrigins, AllowedHeaders};
use rocket::{
    Request,
    Response
};

use rocket_contrib::Json;

// type definitions for error handling
mod errors {
    use rocket;
    error_chain!{
        foreign_links {
            DieselError(::diesel::result::Error);
        }
        errors { 
            RocketError(error: rocket::error::LaunchError) {
                description("Rocket web server failed to run")
                display("{}", error)
            }
        }
    }

    use rocket::response::Responder;
    use rocket::Request;
    use rocket::Response;
    impl<'a> Responder<'a> for Error {
        fn respond_to(self, _: &Request) -> ::std::result::Result<Response<'a>,rocket::http::Status> {
            Err(rocket::http::Status::BadRequest)
        }
    }
}

pub use errors::*;

#[get("/products")]
fn get_products(db_conn: db::DbConn) -> Result<Json<Vec<models::Product>>> {
    let products = db::get_all_products(&*db_conn)
        .chain_err(|| "unable to get products from database")?;
    
    Ok(Json(products))
}

// The following two endpoints are only for UI purposes,
// not actually used in the process of making a purchase

#[get("/cards/<code>/info")]
fn get_user_info(db_conn: db::DbConn, code: String) -> Result<Json<models::User>> {
    let user = db::get_user_by_card(&*db_conn, code)
        .chain_err(|| "unable to get user by card from db")?;

    Ok(Json(user))
}

#[get("/products/by_code/<code>/info")]
fn get_product_info_by_code(db_conn: db::DbConn, code: String) -> Result<Json<models::Product>> {
    let product = db::get_product_info_by_code(&*db_conn, code)
        .chain_err(|| "unable to get product info by code")?;

    Ok(Json(product))
}

// This is the critical one
#[post("/users/<id>/purchase", data="<shopping_cart>")]
fn make_purchase(db_conn: db::DbConn, id: i32, shopping_cart: Json<Vec<i32>>) -> Result<()> {
    let price: i32 = shopping_cart.into_inner().iter()
        .fold(Ok(0), |acc: Result<i32>, val| {
            acc.and_then(|val| Ok(val + db::get_product_info(&*db_conn, val)
                .chain_err(|| "unable to get price of product")?
                .price))
        })?;

    //TODO: apply penalty factor here

    db::deduct_money_amount(&*db_conn, id, price)
        .chain_err(|| "unable to deduct price from user's balance")?;

    Ok(())
}

pub mod admin {
    use super::*;

    #[post("/admin/product", data="<product>")]
    fn new_product(db_conn: db::DbConn, product: Json<models::NewProduct>) -> Result<Json<models::Product>> {
        let new_product = product.into_inner();

        let product = db::create_product(&*db_conn, new_product)
            .chain_err(|| "insert product failed")?;

        Ok(Json(product))
    }

    #[put("/admin/product/<id>", data="<product>")]
    fn update_product(db_conn: db::DbConn, id: i32, product: Json<models::NewProduct>) -> Result<Json<models::Product>> {
        let new_product = product.into_inner();

        let product = db::update_product(&*db_conn, id, new_product)
            .chain_err(|| "update product failed")?;

        Ok(Json(product))
    }

    #[delete("/admin/product/<id>")]
    fn delete_product(db_conn: db::DbConn, id: i32) -> Result<()> {
        db::delete_product(&*db_conn, id)
            .chain_err(|| "delete product failed")?;

        Ok(())
    }

    #[get("/admin/users")]
    fn get_users(db_conn: db::DbConn) -> Result<Json<Vec<models::User>>> {
        let users = db::get_all_users(&*db_conn)
            .chain_err(|| "unable to get users from database")?;
        
        Ok(Json(users))
    }

    #[post("/admin/users", data="<user>")]
    fn new_user(db_conn: db::DbConn, user: Json<models::NewUser>) -> Result<Json<models::User>> {
        let new_user = user.into_inner();

        let user = db::create_user(&*db_conn, new_user)
            .chain_err(|| "insert user failed")?;

        Ok(Json(user))
    }

    #[put("/admin/users/<id>", data="<user>")]
    fn update_user(db_conn: db::DbConn, id: i32, user: Json<models::NewUser>) -> Result<Json<models::User>> {
        let new_user = user.into_inner();

        let user = db::update_user(&*db_conn, id, new_user)
            .chain_err(|| "update user failed")?;

        Ok(Json(user))
    }

    #[delete("/admin/users/<id>")]
    fn delete_user(db_conn: db::DbConn, id: i32) -> Result<()> {
        db::delete_user(&*db_conn, id)
            .chain_err(|| "delete user failed")?;

        Ok(())
    }

    #[post("/admin/cards", data="<card>")]
    fn new_card(db_conn: db::DbConn, card: Json<models::Card>) -> Result<()> {
        let new_card = card.into_inner();

        db::create_card(&*db_conn, new_card)
            .chain_err(|| "insert card failed")?;

        Ok(())
    }

    #[delete("/admin/cards/<code>")]
    fn delete_card(db_conn: db::DbConn, code: String) -> Result<()> {
        db::delete_card(&*db_conn, code)
            .chain_err(|| "delete card failed")?;

        Ok(())
    }

    #[get("/admin/users/<id>/cards")]
    fn get_cards_by_user(db_conn: db::DbConn, id: i32) -> Result<Json<Vec<models::Card>>> {
        let cards = db::get_cards_by_user(&*db_conn, id)
            .chain_err(|| "get cards failed")?;

        Ok(Json(cards))
    }
}

fn run() -> Result<()> {
    let options = rocket_cors::Cors {
        ..Default::default()
    };

    Err(Error::from_kind(ErrorKind::RocketError(
        rocket::ignite()
            .mount("/", routes![
                   get_products,

                   admin::new_product,
                   admin::update_product,
                   admin::delete_product,

                   admin::get_users,
                   admin::new_user,
                   admin::update_user,
                   admin::delete_user,

                   admin::new_card,
                   admin::delete_card,
                   admin::get_cards_by_user
            ])
            .manage(db::init_pool()?)
            .attach(options)
            .launch())))
}

// error_chain -defined main() function
quick_main!(run);
